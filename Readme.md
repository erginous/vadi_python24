VADI (Voice-Automated Dialog Intelligence) Project

How to run this project ?

create anaconda environment with python version = 3.10.0

conda create --name VADI python==3.10.0

Activate environment:
conda activate VADI

Install dependencies:
pip install -r requirements.txt

To generate reports we only need to run main.py file . We need to mention CSV folder and csv file (Which is session_id).
python main.py


About files:
summary.py : Generate summary report and data for Basicsummary flowchart.
summary_graph.py : Generate BasicSummary Flow Chart report
topic_words_wordcloud.py : Generate keywords and Sunburst chart report . Also generate wordcloud from these keywords.
important_parti_statements.py : Generate important participants statements report
sentiment.py - Sentiment analysis and its visualization , Large utterance path data.csv is generated


CSV_files folder contains .csv files.
Generated reports will be stored in output/session_id directory






Installation Guide:

* sudo apt install uvicorn
* sudo apt install ffmpeg
* sudo apt-get install graphviz

